package com.epi.sample;

/**
 * Created by olivier on 24/03/15.
 */
public class EntryPoint {

    public static void main(String[] args) throws Exception{
        Animal animal = new Animal("roger", "blanc");

        AnimalIdentityBuilder.buildIdentityFile(animal);
    }
}
