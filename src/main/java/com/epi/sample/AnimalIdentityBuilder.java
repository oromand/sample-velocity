package com.epi.sample;

import java.io.*;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

/**
 *
 */
public class AnimalIdentityBuilder
{

    public static void buildIdentityFile(Object animal) throws Exception {
        /*  first, get and initialize an engine  */
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();

        /*  create a context and add data */
        VelocityContext context = new VelocityContext();
        /* */
        context.put("animal", animal);

        /* now render the template into a StringWriter */
        FileWriter w = new FileWriter("animal.txt");
        Velocity.mergeTemplate("AnimalIdentity.vm", "UTF-8", context, w);
        w.close();
    }
}
